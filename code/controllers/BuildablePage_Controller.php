<?php
/**
 * Buildable Page controller.
 *
 * Rey Benher <r.benher@we-are-mea.com>
 */
class BuildablePage_Controller extends Page_Controller {
	/**
	 * Initialize controller.
	 */
	public function init() {
		parent::init();
	}
}
