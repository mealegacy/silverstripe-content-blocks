<?php
/**
 * Buildable Page entity.
 *
 * Rey Benher <r.benher@we-are-mea.com>
 */
class BuildablePage extends Page {
	/**
	 * Page entity icon.
	 */
	private static $icon = array('silverstripe-content-blocks/assets/images/icons/build-icon-16.png');

	/**
	 * Page name.
	 */
	private static $singular_name = 'Buildable Page';

	/**
	 * Page description.
	 */
	private static $description = 'Uses content blocks to build a page.';

	/**
	 * DB Fields.
	 */
	private static $db = array();

	/**
	 * Many many.
	 */
	private static $many_many = array(
		'ContentBlocks' => 'ContentBlock',
	);

	/**
	 * Method to show CMS fields for creating or updating.
	 */
	public function getCMSFields() {
		$fields = parent::getCMSFields();

		// Remove default Content field
		$fields->removeByName('Content');

		// Content Blocks
		$gridFieldConfig = \ContentBlock_GridFieldConfig::create();
		$contentBlocksGridField = GridField::create('ContentBlock', _t('ContentBlock.Blocks', 'Blocks'), $this->ContentBlocks(), $gridFieldConfig);
		$contentBlocksHeaderField = HeaderField::create('ContentBlocksHeading', _t('ContentBlock.ContentBlocksHeading', 'Content Blocks'), 3);
		$contentBlocksField = CompositeField::create($contentBlocksHeaderField, $contentBlocksGridField);
		$fields->insertAfter($contentBlocksField, 'MenuTitle');

		return $fields;
	}

	/**
	 * Get Content Blocks.
	 *
	 * @return DataList list of content blocks
	 */
	public function ContentBlocks() {
		return $this->getManyManyComponents('ContentBlocks')
			->sort('SortOrder');
	}
}
