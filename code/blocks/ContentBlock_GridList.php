<?php
/**
 * Content Block Grid List class.
 *
 * Rey Benher <r.benher@we-are-mea.com>
 */
class ContentBlock_GridList extends ContentBlock_GridContent {
	/**
	 * Singular name.
	 */
	private static $singular_name = 'Grid List';

	/**
	 * Plural name.
	 */
	private static $plural_name = 'Grid Lists';

	/**
	 * DB fields.
	 */
	private static $db = array(
		'CountXS' => 'Int',
		'CountSM' => 'Int',
		'CountMD' => 'Int',
		'CountLG' => 'Int',
		'CountXL' => 'Int',
		'Visible' => 'Boolean',
	);

	/**
	 * Many many.
	 */
	private static $many_many = array(
		'GridListItems' => 'GridListItem',
	);

	/**
	 * Many many extra fields.
	 */
	private static $many_many_extraFields = array(
		'GridListItems' => array(
			'SortOrder' => 'Int',
		),
	);

	/**
	 * Input field labels.
	 *
	 * @param bool $includeRelations a boolean value to indicate if the labels returned include relation fields
	 *
	 * @return array $labels array of field labels
	 */
	public function fieldLabels($includerelations = true) {
		$labels = parent::fieldLabels($includerelations);

		$labels['GridListItems'] = $this->getTranslatedString('GridListItems', 'Grid List Items');

		return $labels;
	}

	/**
	 * Method to show CMS fields for creating or updating.
	 */
	public function getCMSFields() {
		$labels = $this->fieldLabels();
		$fields = parent::getCMSFields();

		// Remove tab for Grid Content items
		$fields->removeByName('GridContentItems');

		// Content Block grid list items
		$gridListItemsTab = Tab::create('TabGridListItems', _t('ContentBlockGridList.TabGridListItems', 'Grid List Items'));
		$fields->insertAfter($gridListItemsTab, 'Main');

		$gridListItemsGridFieldConfig = \ContentBlock_GridFieldConfig::create();
		$gridListItemsGridField = GridField::create('GridListItems', $labels['GridListItems'], $this->GridListItems(), $gridListItemsGridFieldConfig);
		$gridListItemsTab->push($gridListItemsGridField);

		return $fields;
	}

	/**
	 * Get Grid List Item elements.
	 */
	public function GridListItems() {
		return $this->getManyManyComponents('GridListItems')
			->sort('SortOrder');
	}
}
