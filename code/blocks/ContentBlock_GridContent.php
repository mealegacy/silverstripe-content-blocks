<?php
/**
 * Content Block Grid List class.
 *
 * Rey Benher <r.benher@we-are-mea.com>
 */
class ContentBlock_GridContent extends ContentBlock {
	/**
	 * Singular name.
	 */
	private static $singular_name = 'Grid Content';

	/**
	 * Plural name.
	 */
	private static $plural_name = 'Grid Content';

	/**
	 * DB fields.
	 */
	private static $db = array(
		'CountXS' => 'Int',
		'CountSM' => 'Int',
		'CountMD' => 'Int',
		'CountLG' => 'Int',
		'CountXL' => 'Int',
		'Visible' => 'Boolean',
	);

	/**
	 * Many many.
	 */
	private static $many_many = array(
		'GridContentItems' => 'GridContentItem',
	);

	/**
	 * Many many extra fields.
	 */
	private static $many_many_extraFields = array(
		'GridContentItems' => array(
			'SortOrder' => 'Int',
		),
	);

	/**
	 * Input field labels.
	 *
	 * @param bool $includeRelations a boolean value to indicate if the labels returned include relation fields
	 *
	 * @return array $labels array of field labels
	 */
	public function fieldLabels($includerelations = true) {
		$labels = parent::fieldLabels($includerelations);

		$labels['GridContentItems'] = $this->getTranslatedString('GridContentItems', 'Grid Content Items');

		return $labels;
	}

	/**
	 * Method to show CMS fields for creating or updating.
	 */
	public function getCMSFields() {
		$labels = $this->fieldLabels();
		$fields = parent::getCMSFields();

		// Content Block content
		$contentField = HtmlEditorField::create('Content', $labels['Content'])
			->setRows(15);
		$fields->addFieldToTab('Root.Main', $contentField);

		// Breakpoints
		$i = 0;
		$breakpointTitles = array_keys($this->getBreakpoints());
		$countFields = array();
		$breakpointColumnCounts = array();
		foreach ($this->getBreakpointColumnCounts() as $columnCount => $cssClassName) {
			$breakpointColumnCounts[$columnCount] = $columnCount;
		}
		foreach ($this->getBreakpoints() as $title => $pixels) {
			$countFields[$title] = DropdownField::create("Count{$title}", "Count for {$title} Breakpoint", $breakpointColumnCounts);
			$countFields[$title]->setDescription("No. of columns for screen breakpoint {$pixels}px");
			$fields->addFieldToTab('Root.Main', $countFields[$title], (($i == 0) ? 'Template' : "Count{$breakpointTitles[$i - 1]}"));
			++$i;
		}

		// Content Block grid list items
		$gridContentItemsTab = Tab::create('TabGridContentItems', $labels['GridContentItems']);
		$fields->insertAfter($gridContentItemsTab, 'Main');

		$gridContentItemsGridFieldConfig = \ContentBlock_GridFieldConfig::create();
		$gridContentItemsGridField = GridField::create('GridContentItems', $labels['GridContentItems'], $this->GridContentItems(), $gridContentItemsGridFieldConfig);
		$gridContentItemsTab->push($gridContentItemsGridField);

		return $fields;
	}

	/**
	 * Get Grid Content Item elements.
	 */
	public function GridContentItems() {
		return $this->getManyManyComponents('GridContentItems')
			->sort('SortOrder');
	}
}
