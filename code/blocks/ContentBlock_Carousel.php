<?php
/**
 * Content Block Carousel class.
 *
 * Rey Benher <r.benher@we-are-mea.com>
 */
class ContentBlock_Carousel extends ContentBlock {
	/**
	 * Singular name.
	 */
	private static $singular_name = 'Carousel Block';

	/**
	 * Plural name.
	 */
	private static $plural_name = 'Carousel Blocks';

	/**
	 * DB fields.
	 */
	private static $db = array(
		'Visible' => 'Boolean',
	);

	/**
	 * Many many.
	 */
	private static $many_many = array(
		'CarouselItems' => 'CarouselItem',
	);

	/**
	 * Many many extra fields.
	 */
	private static $many_many_extraFields = array(
		'CarouselItems' => array(
			'SortOrder' => 'Int',
		),
	);

	/**
	 * Method to show CMS fields for creating or updating.
	 */
	public function getCMSFields() {
		$labels = $this->fieldLabels();
		$fields = parent::getCMSFields();

		// Content Block content
		$contentField = HtmlEditorField::create('Content', $labels['Content'])
			->setRows(15);
		$fields->addFieldToTab('Root.Main', $contentField);

		// Tab for carousel items
		$carouselItemsTab = Tab::create('TabCarouselItems', _t('ContentBlockCarousel.TabCarouselItems', 'Carousel Items'));
		$fields->insertAfter($carouselItemsTab, 'Main');

		// Content Block carousel items
		$carouselItemsGridFieldConfig = \ContentBlock_GridFieldConfig::create();
		$carouselItemsGridField = GridField::create('CarouselItems', _t('ContentBlockCarousel.CarouselItems', 'Carousel Items'), $this->CarouselItems(), $carouselItemsGridFieldConfig);
		$carouselItemsTab->push($carouselItemsGridField);

		return $fields;
	}

	/**
	 * Get Carousel Item elements.
	 */
	public function CarouselItems() {
		return $this->getManyManyComponents('CarouselItems')
			->sort('SortOrder');
	}
}
