<?php
/**
 * Content Block base class.
 *
 * Rey Benher <r.benher@we-are-mea.com>
 */
class ContentBlock extends DataObject {
	/**
	 * Class string constants.
	 */
	const VISIBILITY_HIDDEN = 'hidden';
	const VISIBILITY_VISIBLE = 'visible';

	/**
	 * Singular name.
	 */
	private static $singular_name = 'Content Block';

	/**
	 * Plural name.
	 */
	private static $plural_name = 'Content Blocks';

	/**
	 * List of custom HTML classes.
	 */
	private static $possible_html_classes = array();

	/**
	 * DB fields.
	 */
	private static $db = array(
		'Title' => 'Varchar(255)',
		'Header' => 'Varchar(255)',
		'Content' => 'HTMLText',
		'Template' => 'Varchar(255)',
		'SortOrder' => 'Int',
		'Visible' => 'Boolean',
	);

	/**
	 * Belongs many many.
	 */
	private static $belongs_many_many = array(
		'Pages' => 'Page',
	);

	/**
	 * Summary fields.
	 */
	private static $summary_fields = array(
		'Title' => 'Title',
		'FormattedClassName' => 'Type',
		'Template' => 'Template',
		'VisibilityStatus' => 'Visibility',
	);

	/**
	 * Searchable fields.
	 */
	private static $searchable_fields = array(
		'ClassName',
		'Title',
	);

	/**
	 * Casting.
	 */
	private static $casting = array(
		'ClassNameCasting' => 'Varchar(255)',
		'CastingHTMLClasses' => 'Text',
	);

	/**
	 * Screen width breakpoints.
	 */
	protected $breakpoints = array(
		'XS' => '<576',
		'SM' => '>=576',
		'MD' => '>=768',
		'LG' => '>=992',
		'XL' => '>=1200',
	);

	/**
	 * Screen width breakpoint column counts.
	 * $breakpointColumnCounts = array(
	 *      $columnCount => $cssClassName,
	 * );.
	 */
	protected $breakpointColumnCounts = array(
		1 => 12,
		2 => 6,
		3 => 4,
		4 => 3,
		6 => 2,
		12 => 1,
	);

	/**
	 * Populate defaults.
	 */
	public function populateDefaults() {
		parent::populateDefaults();
	}

	/**
	 * Input field labels.
	 *
	 * @param bool $includeRelations a boolean value to indicate if the labels returned include relation fields
	 *
	 * @return array $labels array of field labels
	 */
	public function fieldLabels($includeRelations = true) {
		$labels = parent::fieldLabels($includeRelations);

		$labels['FormattedNiceName'] = $this->getTranslatedString('ClassName', 'Type');
		$labels['ClassName'] = $labels['FormattedNiceName'];
		$labels['Title'] = $this->getTranslatedString('Title', 'Title');
		$labels['Header'] = $this->getTranslatedString('Header', 'Header');
		$labels['Content'] = $this->getTranslatedString('Content', 'Content');

		return $labels;
	}

	/**
	 * Method to show CMS fields for creating or updating.
	 */
	public function getCMSFields() {
		$labels = $this->fieldLabels();

		// Main Tab
		$tabMain = Tab::create('Main', _t('SiteTree.TABMAIN', 'Main'));
		$root = TabSet::create('Root', $tabMain)
			->setTemplate('CMSTabSet');

		// Content Block title
		$tabMain->push(TextField::create('Title', $labels['Title']));

		if ($this->ID <= 0) {
			// Content Block type
			$blockTypeDropdownField = DropdownField::create('ClassNameCasting', $labels['ClassName'], $this->getClassNames())
				->addExtraClass('no-change-track');
			$tabMain->insertBefore($blockTypeDropdownField, 'Title');

			// Content Block helper message
			$blockTypeHelperMessage = _t('ContentBlock.SaveToDisplayMoreOptionsMessage', 'You will see more options after you save.');
			$tabMain->push(LiteralField::create('ContentBlockTypeMessage', '<p class="message notice">'.$blockTypeHelperMessage.'</p>'));
		} else {
			// Content Block Header
			$blockHeaderField = TextField::create('Header', $labels['Header'])
				->setDescription(_t('ContentBlock.Optional', 'Optional'));
			$tabMain->push($blockHeaderField);

			// Content Block custom HTML classes
			$htmlClasses = $this->possibleHTMLClasses();

			if ($htmlClasses && is_array($htmlClasses) && !empty($htmlClasses)) {
				// Tag Field to choose possible HTML classes from list
				$htmlClassesExisting = explode(' ', $this->getField('HTMLClasses'));
				$htmlClassesField = StringTagField::create('CastingHTMLClasses', _t('ContentBlock.CustomHTMLClasses', 'HTML classes'), $htmlClasses, $htmlClassesExisting);
				$tabMain->push($htmlClassesField);
			} else {
				// Text Field to add custom HTML classes
				$htmlClassesField = TextField::create('ManyMany[HTMLClasses]', _t('ContentBlock.CustomHTMLClasses', 'HTML classes'))
					->setDescription(_t('ContentBlock.CustomHTMLClassesMessage', 'Optional, custom HTML classes, separate by spaces.'));
			}
			$tabMain->push($htmlClassesField);

			// Add dropdown field for selecting template
			$templatesList = $this->stat('templates');
			$formattedTemplatesList = array();

			if (isset($templatesList[$this->ClassName]) && is_array($templatesList[$this->ClassName]) && !empty($templatesList[$this->ClassName])) {
				foreach ($templatesList[$this->ClassName] as $template => $labelData) {
					$templateLabel = $template;

					if (isset($labelData['label']) && $labelData['label']) {
						$templateLabel = $labelData['label'];
					}

					if (isset($labelData['label_translation_str']) && $labelData['label_translation_str']) {
						$templateLabel = _t($labelData['label_translation_str'], $templateLabel);
					}

					$formattedTemplatesList[$template] = $templateLabel;
				}

				asort($formattedTemplatesList);
			}

			// Template style for Content Block
			$templateField = DropdownField::create('Template', _t('ContentBlock.Template', 'Block Style'), $formattedTemplatesList)
				->setEmptyString(_t('ContentBlock.TemplateEmptyString', '- Default -'));
			$tabMain->push($templateField);

			// General Options
			$generalOptionsGroup = FieldGroup::create();
			$generalOptionsGroup->setTitle('General Options');

			// General Options: Visibility
			$visibleField = CheckboxField::create('Visible', 'Visible on the site');
			if (!$this->ID) {
				$visibleField->setValue(true);
			}
			$generalOptionsGroup->push($visibleField);

			$tabMain->insertAfter('Template', $generalOptionsGroup);
		}

		// Extend fields
		$fields = FieldList::create($root);

		// Allow extensions
		$this->extend('updateCMSFields', $fields);

		return $fields;
	}

	/**
	 * Validate and save custom fields on before write.
	 */
	public function onBeforeWrite() {
		parent::onBeforeWrite();

		// Validate title
		if (trim($this->Title) == '') {
			throw new ValidationException(ValidationResult::create(false, _t('ContentBlock.ValidationErrorEmptyTitle', 'Title is required')));
		}

		// Validate class name
		$this->ClassName = $this->ClassNameCasting;
		if (trim($this->ClassName) == '') {
			throw new ValidationException(ValidationResult::create(false, _t('ContentBlock.ValidationErrorEmptyClassName', 'Type is required')));
		}

		// Save sort order position
		if (!$this->SortOrder) {
			$this->SortOrder = self::get()->max('SortOrder') + 1;
		}

		// Save custom HTML classes
		$htmlClasses = $this->possibleHTMLClasses();
		if (is_array($htmlClasses) && !empty($htmlClasses)) {
			$htmlClasses = $this->CastingHTMLClasses;
			$htmlClasses = $htmlClasses ? implode(' ', explode(',', $htmlClasses)) : '';
			$this->setField('ManyMany[HTMLClasses]', $htmlClasses);
		}
	}

	/**
	 * Custom template to output data.
	 */
	public function forTemplate() {
		$view = new \SSViewer($this->getTemplateFileNames());

		return $view->process($this->customise($this));
	}

	/**
	 * Return a list of class names for templates.
	 *
	 * @return array
	 */
	protected function getClassNames() {
		$classesList = ClassInfo::getValidSubClasses('ContentBlock');
		$formattedClassesList = array();
		foreach ($classesList as $className) {
			$classInstance = singleton($className);

			// Check if class is not an instance of HiddenClass
			if ($this->ClassName !== $classInstance->ClassName) {
				if ($classInstance instanceof HiddenClass) {
					continue;
				}
			}

			// Check if permissions required are available
			$permissions = $classInstance->stat('need_permission');
			if ($permissions) {
				if (!$this->can($permissions)) {
					continue;
				}
			}

			$singularName = $classInstance->i18n_singular_name();
			$formattedClassesList[$className] = $singularName;

			if (i18n::get_lang_from_locale(i18n::get_locale()) != 'en') {
				$formattedClassesList[$className] = $formattedClassesList[$singularName]." ({$singularName})";
			}
		}

		// Sort class names list alphabetically
		asort($formattedClassesList);

		// Remove base class from the list
		if (count($formattedClassesList) && isset($formattedClassesList['ContentBlock'])) {
			unset($formattedClassesList['ContentBlock']);
		}

		return $formattedClassesList;
	}

	/**
	 * Get all possible template names.
	 */
	public function getTemplateFileNames() {
		$templatesList = array();

		// Add static class name with template variation
		if ($this->Template) {
			array_push($templatesList, $this->ClassName.'_'.$this->Template);
		}

		// Add static class name
		array_push($templatesList, $this->ClassName);

		$this->extend('updateTemplateFileNames', $templatesList);

		return $templatesList;
	}

	/**
	 * Get display titles for adding existing Content Blocks.
	 */
	public function getExistingSearchItemTitle() {
		return "<strong>{$this->getTitle()}</strong> (Type: {$this->ClassName}. Template: {$this->getTemplateName()})";
	}

	/**
	 * Get all possible HTML classes.
	 */
	public function possibleHTMLClasses() {
		$classes = $this->stat('possible_html_classes');

		$this->extend('updatePossibleHTMLClasses', $classes);

		return $classes;
	}

	/**
	 * Get the current template name.
	 */
	public function getTemplateName() {
		return $this->Template ? $this->Template : 'Default';
	}

	/**
	 * Get grid column width based on count.
	 *
	 * @param null $breakpoint
	 *
	 * @return string class name (count) for given breakpoint
	 */
	public function ColumnCountClass($breakpoint = null) {
		$breakpointColumnCounts = $this->getBreakpointColumnCounts();

		$columnCount = $this->getField("Count{$breakpoint}");
		if (!$columnCount) {
			return null;
		}

		if (!isset($breakpointColumnCounts[$columnCount])) {
			return null;
		}

		return $breakpointColumnCounts[$columnCount];
	}

	/**
	 * Get the screen breakpoints.
	 */
	public function getBreakpoints() {
		return $this->breakpoints;
	}

	/**
	 * Get the screen breakpoint column counts.
	 */
	public function getBreakpointColumnCounts() {
		return $this->breakpointColumnCounts;
	}

	/**
	 * Entity's visibility status to hide it on the front end.
	 *
	 * @return string visibility status
	 */
	public function getVisibilityStatus() {
		return ucfirst($this->owner->getField('Visible') ? static::VISIBILITY_VISIBLE : static::VISIBILITY_HIDDEN);
	}

	/**
	 * Get translated string value.
	 *
	 * @return string translated string
	 */
	public function getTranslatedString($fieldName, $defaultValue = null) {
		return _t("{$this->getTranslationPrefix()}.{$fieldName}", $defaultValue ?: $fieldName);
	}

	/**
	 * Get translation prefix for class.
	 *
	 * @return string translation prefix
	 */
	public function getTranslationPrefix() {
		return $this->getFormattedClassName();
	}

	/**
	 * Get formatted name for class.
	 *
	 * @return string formatter class name
	 */
	public function getFormattedClassName() {
		return DBField::create_field('Text', singleton($this->class)->i18n_singular_name());
	}
}
