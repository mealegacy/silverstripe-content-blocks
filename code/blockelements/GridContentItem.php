<?php
/**
 * Grid List Item.
 *
 * Rey Benher <r.benher@we-are-mea.com>
 */
class GridContentItem extends ContentBlockItem {
	/**
	 * Singular name.
	 */
	private static $singular_name = 'Grid Content Item';

	/**
	 * Plural name.
	 */
	private static $plural_name = 'Grid Content Items';

	/**
	 * DB fields.
	 */
	private static $db = array(
		'Title' => 'Varchar(255)',
		'Header' => 'Varchar(255)',
		'Content' => 'HTMLText',
		'Visible' => 'Boolean',
	);

	/**
	 * Summary fields.
	 */
	private static $summary_fields = array(
		'Title',
		'Header',
	);

	/**
	 * Searchable fields.
	 */
	private static $searchable_fields = array(
		'Title',
		'Header',
	);

	/**
	 * Field labels.
	 */
	public function fieldLabels($includerelations = true) {
		$labels = parent::fieldLabels($includerelations);

		$labels['Header'] = $this->getTranslatedString('Header');

		return $labels;
	}

	/*
	 * Method to show CMS fields for creating or updating.
	 */
	public function getCMSFields() {
		$labels = $this->fieldLabels();
		$fields = parent::getCMSFields();

		// Title field
		$fields->unshift(TextField::create('Title', $labels['Title']));

		// Header field
		$fields->insertAfter(TextField::create('Header', $labels['Header']), 'Title');

		// Content field
		$contentField = HtmlEditorField::create('Content', $labels['Content']);
		$contentField->setRows(10);
		$fields->insertAfter($contentField, 'GeneralOptions');

		return $fields;
	}
}
