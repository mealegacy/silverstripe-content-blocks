<?php
/**
 * Carousel Item.
 *
 * Rey Benher <r.benher@we-are-mea.com>
 */
class ContentBlockItem extends DataObject {
	/**
	 * Class string constants.
	 */
	const VISIBILITY_HIDDEN = 'hidden';
	const VISIBILITY_VISIBLE = 'visible';

	/**
	 * Populate defaults.
	 */
	public function populateDefaults() {
		parent::populateDefaults();
	}

	/**
	 * Input field labels.
	 *
	 * @param bool $includeRelations a boolean value to indicate if the labels returned include relation fields
	 *
	 * @return array $labels array of field labels
	 */
	public function fieldLabels($includerelations = true) {
		$labels = parent::fieldLabels($includerelations);

		$labels['Title'] = $this->getTranslatedString('Title');
		$labels['Content'] = $this->getTranslatedString('Content');
		$labels['Image'] = $this->getTranslatedString('Image');
		$labels['ImageThumb'] = $labels['Image'];
		$labels['URL'] = $this->getTranslatedString('URL');

		return $labels;
	}

	/**
	 * Method to show CMS fields for creating or updating.
	 */
	public function getCMSFields() {
		// General Options
		$generalOptionsGroup = FieldGroup::create();
		$generalOptionsGroup->setTitle('General Options');
		$generalOptionsGroup->setID('GeneralOptions');

		// General Options: Visibility
		$visibleField = CheckboxField::create('Visible', 'Visible on the site');
		if (!$this->ID) {
			$visibleField->setValue(true);
		}
		$generalOptionsGroup->push($visibleField);

		return FieldList::create(
			$generalOptionsGroup
		);
	}

	/**
	 * Entity's visibility status to hide it on the front end.
	 *
	 * @return string visibility status
	 */
	public function getVisibilityStatus() {
		return ucfirst($this->owner->getField('Visible') ? static::VISIBILITY_VISIBLE : static::VISIBILITY_HIDDEN);
	}

	/**
	 * Get translated string value.
	 *
	 * @return string translated string
	 */
	public function getTranslatedString($fieldName, $defaultValue = null) {
		return _t("{$this->getTranslationPrefix()}.{$fieldName}", $defaultValue ?: $fieldName);
	}

	/**
	 * Get translation prefix for class.
	 *
	 * @return string translation prefix
	 */
	public function getTranslationPrefix() {
		return $this->getFormattedClassName();
	}

	/**
	 * Get formatted name for class.
	 *
	 * @return string formatter class name
	 */
	public function getFormattedClassName() {
		return DBField::create_field('Text', singleton($this->class)->i18n_singular_name());
	}
}
