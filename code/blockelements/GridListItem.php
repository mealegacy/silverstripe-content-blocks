<?php
/**
 * Grid List Item.
 *
 * Rey Benher <r.benher@we-are-mea.com>
 */
class GridListItem extends ContentBlockItem {
	/**
	 * Singular name.
	 */
	private static $singular_name = 'Grid List Item';

	/**
	 * Plural name.
	 */
	private static $plural_name = 'Grid List Items';

	/**
	 * DB fields.
	 */
	private static $db = array(
		'Title' => 'Varchar(255)',
		'Header' => 'Varchar(255)',
		'ButtonText' => 'Varchar(255)',
		'ButtonVisible' => 'Boolean',
		'Content' => 'HTMLText',
		'ExternalURL' => 'Varchar(255)',
		'Visible' => 'Boolean',
	);

	/**
	 * Has one.
	 */
	private static $has_one = array(
		'Image' => 'Image',
		'BackgroundImage' => 'Image',
		'InternalPage' => 'SiteTree',
	);

	/**
	 * Summary fields.
	 */
	private static $summary_fields = array(
		'Title',
		'Header',
		'URL',
		'ImageThumb',
	);

	/**
	 * Searchable fields.
	 */
	private static $searchable_fields = array(
		'Title',
		'Header',
	);

	/**
	 * Field labels.
	 */
	public function fieldLabels($includerelations = true) {
		$labels = parent::fieldLabels($includerelations);

		$labels['Header'] = $this->getTranslatedString('Header');
		$labels['ExternalURL'] = $this->getTranslatedString('ExternalURL', 'External URL');
		$labels['InternalURL'] = $this->getTranslatedString('InternalURL', 'Internal URL');
		$labels['ButtonText'] = $this->getTranslatedString('ButtonText', 'Button Text');
		$labels['ButtonVisible'] = $this->getTranslatedString('ButtonVisible', 'Button visible on the Site');

		return $labels;
	}

	/*
	 * Method to show CMS fields for creating or updating.
	 */
	public function getCMSFields() {
		$labels = $this->fieldLabels();
		$fields = parent::getCMSFields();

		// Title field
		$fields->unshift(TextField::create('Title', $labels['Title']));

		// Header field
		$fields->insertAfter(TextField::create('Header', $labels['Header']), 'Title');

		// URL fields
		$fields->insertAfter(TreeDropdownField::create('InternalPageID', $labels['InternalURL'], 'SiteTree'), 'Header');
		$fields->insertAfter(TextField::create('ExternalURL', $labels['ExternalURL']), 'InternalPageID');

		// Button fields
		// Button: Text field
		$fields->insertAfter(TextField::create('ButtonText', $labels['ButtonText']), 'ExternalURL');

		// Button: Options group
		$buttonOptionsGroup = FieldGroup::create();
		$buttonOptionsGroup->setTitle('Button Options');
		$buttonOptionsGroup->setID('ButtonOptions');

		// Button: Visibility
		$buttonVisibleField = CheckboxField::create('ButtonVisible', $labels['ButtonVisible']);
		if (!$this->ID) {
			$buttonVisibleField->setValue(true);
		}
		$buttonOptionsGroup->push($buttonVisibleField);
		$fields->insertAfter($buttonOptionsGroup, 'ButtonText');

		// Content field
		$contentField = HtmlEditorField::create('Content', $labels['Content']);
		$contentField->setRows(10);
		$fields->insertAfter($contentField, 'ButtonOptions');

		// Image field
		$imageField = UploadField::create('Image', $labels['Image'])
			->setFolderName('Uploads/GridListItems/')
			->setAllowedExtensions(array(
				'gif',
				'jpg',
				'jpeg',
				'png',
				'svg',
			));
		$fields->insertAfter($imageField, 'Content');

		return $fields;
	}

	/**
	 * Alias for returning button title.
	 *
	 * @param string $defaultTitle default button title
	 *
	 * @return string button title
	 */
	public function ButtonTitle($defaultTitle = 'More Info') {
		return $this->getButtonTitle($defaultTitle);
	}

	/**
	 * Return button title.
	 *
	 * @param string $defaultTitle default button title
	 *
	 * @return string button title
	 */
	public function getButtonTitle($defaultTitle = 'More Info') {
		return $this->ButtonText ? $this->ButtonText : $defaultTitle;
	}

	/**
	 * Get list item URL.
	 */
	protected function getURL() {
		if ($this->ExternalURL) {
			return $this->ExternalURL;
		}

		return $this->InternalPageID ? $this->InternalPage()->Link() : null;
	}

	/**
	 * Image thumbnail for gridfield.
	 */
	protected function getImageThumb() {
		return $this->ImageID ? $this->Image()->setWidth(60) : '';
	}
}
