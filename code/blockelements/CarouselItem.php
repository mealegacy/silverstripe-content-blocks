<?php
/**
 * Carousel Item.
 *
 * Rey Benher <r.benher@we-are-mea.com>
 */
class CarouselItem extends ContentBlockItem {
	/**
	 * Singular name.
	 */
	private static $singular_name = 'Carousel Item';

	/**
	 * Plural name.
	 */
	private static $plural_name = 'Carousel Items';

	/**
	 * DB fields.
	 */
	private static $db = array(
		'Title' => 'Varchar(255)',
		'URL' => 'Varchar(255)',
		'Visible' => 'Boolean',
	);

	/**
	 * Has one.
	 */
	private static $has_one = array(
		'Image' => 'Image',
	);

	/**
	 * Summary fields.
	 */
	private static $summary_fields = array(
		'Title' => 'Title',
		'URL' => 'URL',
		'ImageThumb' => 'Thumbnail',
		'VisibilityStatus' => 'Visibility',
	);

	/**
	 * Searchable fields.
	 */
	private static $searchable_fields = array(
		'Title',
		'URL',
	);

	/**
	 * Method to show CMS fields for creating or updating.
	 */
	public function getCMSFields() {
		$labels = $this->fieldLabels();
		$fields = parent::getCMSFields();

		// Title field
		$fields->unshift(TextField::create('Title', $labels['Title']));

		// URL field
		$fields->insertAfter(TextField::create('URL', $labels['URL']), 'Title');

		// Image field
		$imageField = UploadField::create('Image', $labels['Image'])
			->setFolderName('Uploads/CarouselItems/')
			->setAllowedExtensions(array(
				'gif',
				'jpg',
				'jpeg',
				'png',
				'svg',
			));
		$fields->push($imageField);

		return $fields;
	}

	/**
	 * Image thumbnail for gridfield.
	 */
	protected function getImageThumb() {
		return $this->ImageID ? $this->Image()->setWidth(60) : '';
	}
}
