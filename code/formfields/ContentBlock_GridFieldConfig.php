<?php
/**
 * Custom GridFieldConfig_RelationEditor field for Content Blocks.
 *
 * Rey Benher <r.benher@we-are-mea.com>
 */
class ContentBlock_GridFieldConfig {
	public static function create($count = 30, $sortField = null, $buttonLabel = 'Add New Block') {
		$gridCfg = \GridFieldConfig_RelationEditor::create($count);

		// Add search button
		$gridCfg->addComponent(new \GridFieldAddExistingSearchButton());

		// Remove autocomplete feature
		$gridCfg->removeComponentsByType('GridFieldAddExistingAutocompleter');

		// Button for adding new content blocks
		$gridCfg->getComponentByType('GridFieldAddNewButton')
			->setButtonName(_t('ContentBlock.AddNewBlock', $buttonLabel));

		if ($sortField && is_string($sortField)) {
			$gridCfg->addComponent(new \GridFieldOrderableRows($sortField));
		}

		return $gridCfg;
	}
}
