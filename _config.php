<?php

/**
 * Default configuration settings for the Content Blocks module.
 *
 * You should not put your own configuration in here rather use your
 * mysite/_config.php file
 */

// Constants
define('CONTENT_BLOCKS_BASE_DIR', basename(dirname(__FILE__)));
