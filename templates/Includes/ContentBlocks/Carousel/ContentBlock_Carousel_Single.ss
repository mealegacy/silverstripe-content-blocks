<div class="content-block content-block--{$ClassName} $Template <% if $HTMLClasses %>{$HTMLClasses}<% end_if %>">
	<%-- Block Header --%>
	<% include ContentBlock_Header %>

	<%-- Carousel Items --%>
	<% include ContentBlock_CarouselItems WrapperClassVariation='single'%>
</div>
