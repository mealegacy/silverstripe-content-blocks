# Silverstripe Content Blocks

Simple, extensible content blocks for Silverstripe 3.x.

[![License](http://img.shields.io/packagist/l/silverstripe/tagfield.svg?style=flat-square)](license.md)

## Requirements

* SilverStripe 3.1 or newer
* Database: MySQL 5+, SQLite3, Postgres 8.3, SQL Server 2008

## Installation

Add the repository details to your project's composer.json as shown below:
```
"repositories": [
	{
		"type": "vcs",
		"url": "git@bitbucket.org:MEAM/silverstripe-content-blocks.git"
	}
]
```

Then simply require the module using composer:

```sh
$ composer require mea/silverstripe-content-blocks
```

## Usage

This module introduces a custom "Buildable Page" type using which you can render
your custom Content Blocks. All the templates included in the module can be
overridden by replicating them in your theme.

### Update your Page.ss template

Include the custom Included Silverstripe template inside your Page.ss to render
the Content Blocks. This can be arranged anywhere on the template where you wish
to display your blocks.

```
<% include ContentBlocks %>
```

### Adding Content Blocks

Once a "Buildable Page" has been created, a GridField which allows creation of
Content Blocks is displayed on the Page Edit form. This is exactly the same as
adding/editing/deleting any custom DataObject from the CMS admin and is very
self-explanatory.

![GridField while editing BuildablePage](docs/images/blocks-page-grid-field.png)
